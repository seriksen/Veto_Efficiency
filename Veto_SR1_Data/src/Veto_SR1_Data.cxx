#include "Veto_SR1_Data.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsVeto_SR1_Data.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

#include <iostream>
#include <fstream>

// Constructor
Veto_SR1_Data::Veto_SR1_Data()
        : Analysis()
{
    m_event->IncludeBranch("eventHeader");
    m_event->IncludeBranch("ss");
    m_event->IncludeBranch("eventTPC");
    m_event->IncludeBranch("pulsesTPC");
    m_event->IncludeBranch("pulsesSkin");
    m_event->IncludeBranch("pulsesODHG");

    m_event->Initialize();

    // Setup logging
    logging::set_program_name("Veto_SR1_Data Analysis");

    // Setup the analysis specific cuts.
    m_cutsVeto_SR1_Data = new CutsVeto_SR1_Data(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();

    // Create Skim
    m_skim = new SkimSvc();


}

// Destructor
Veto_SR1_Data::~Veto_SR1_Data()
{
    delete m_cutsVeto_SR1_Data;
    delete m_skim;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void Veto_SR1_Data::Initialize()
{
    INFO("Initializing Veto_SR1_Data Analysis");

    m_cuts->sr1()->Initialize();

    m_skim->InitializeSkim(m_event, "skim_Veto_SR1_Data.root");

}

// Execute() - Called once per event.
void Veto_SR1_Data::Execute()
{

    n_events++;

    DEBUG("New Event");

    // Run through cuts
    if ( (*m_event->m_singleScatter)->nSingleScatters > 0 && (*m_event->m_tpcPulses)->nPulses > 0 ){

        DEBUG("Passed SS");

        n_ss_events++;

        s1Area = (*m_event->m_singleScatter)->s1Area_phd;
        s2Area = (*m_event->m_singleScatter)->s2Area_phd;

        s1ID = (*m_event->m_singleScatter)->s1PulseID;
        s2ID = (*m_event->m_singleScatter)->s2PulseID;

        // Livetime cuts //
        cutMuonVeto = m_cuts->sr1()->TPCMuonVeto();
        cutBuffers = m_cuts->sr1()->BufferStartTime() && m_cuts->sr1()->BufferStopTime(s1ID,s2ID);
        cutExcessArea = m_cuts->sr1()->PassExcessArea();
        cutBurst = m_cuts->sr1()->PassODBurstNoiseTag();
        livetimeCuts = cutMuonVeto && cutExcessArea && cutBurst && cutBuffers;

        // Physics cuts //
        //skinCut = m_cutsVeto_SR1_Data->SkinCut(skin_pulse_starts, gamma_flash_time);
        roiCut = m_cutsVeto_SR1_Data->ROI(s1Area, s2Area);
        // fiducialCut = m_cuts->sr1()->Fiducial();
        physicsCuts = roiCut;// && skinCut;

        // S1 Cuts //
        cutS1Prom = m_cuts->sr1()->ProminenceCut_SS();
        cutStinger = m_cuts->sr1()->Stinger(s1ID);
        cutS1TBA = m_cuts->sr1()->S1TBA_SS();
        cutHSC = m_cuts->sr1()->PassHSCCut(s1ID);
        cutChanTimingS1 = m_cuts->sr1()->PassS1ChannelTiming(s1ID);
        s1Cuts = cutS1Prom && cutStinger && cutS1TBA && cutHSC && cutChanTimingS1;

        // S2 Cuts //
        cutS2Width = m_cuts->sr1()->S2Width_SS();
        cutNarrowS2 = m_cuts->sr1()->NarrowS2Cut();
        cutEarlyPeakS2 = m_cuts->sr1()->S2EarlyPeakCut();
        cutXY = m_cuts->sr1()->IsValidXY(s2ID);
        cutS2TBA = m_cuts->sr1()->GasS2TBACut();
        s2Cuts = cutS2Width && cutNarrowS2 && cutEarlyPeakS2 && cutXY && cutS2TBA;

        // allCuts = livetimeCuts && physicsCuts && s1Cuts && s2Cuts;
        allCuts = livetimeCuts && s1Cuts && s2Cuts && physicsCuts;

        DEBUG("Cut summary");
        DEBUG("Livetime: " + std::to_string(livetimeCuts));
        DEBUG("Physics: " + std::to_string(physicsCuts));
        DEBUG("S1: " + std::to_string(s1Cuts));
        DEBUG("S2: " + std::to_string(s2Cuts));
        DEBUG("All: " + std::to_string(allCuts));

        if (allCuts) {
            DEBUG("All Cuts");
            // Plot S1 vs S2 to make sure correct NR band is used
            m_hists->BookFillHist("S1-vs-logS2", 300, 0, 300, 50, 2.5, 5.5, s1Area, log10(s2Area));
            sigma = m_cutsVeto_SR1_Data->CalculateNRBandDistance_SR1(s1Area, log10(s2Area));

            if (sigma < 1 && sigma > -1) {
                DEBUG("Sigma Cuts");
                m_hists->BookFillHist("sigma-S1-vs-logS2", 300, 0, 300, 50, 2.5, 5.5, s1Area, log10(s2Area));
                m_skim->SkimEvent(m_event);
                n_final_events++;

                // Now save event
                //m_skim->SkimEvent(m_event);
            }

        }

    } // if TPC SS

    m_cuts->sr1()->DeleteInactiveProgenitors();

}

// Finalize() - Called once after event loop.
void Veto_SR1_Data::Finalize()
{
    INFO("Finalizing Veto_SR1_Data Analysis");

    std::cout << "Processing Summary" << std::endl;
    std::cout << "--------------" << std::endl;
    std::cout << "n_events:" << n_events << std::endl;
    std::cout << "n_ss_events:" << n_ss_events << std::endl;
    std::cout << "n_final_events:" << n_final_events << std::endl;
    std::cout << "--------------" << std::endl;
}
