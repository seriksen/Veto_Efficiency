#ifndef Veto_SR1_Data_H
#define Veto_SR1_Data_H

#include "Analysis.h"

#include "CutsVeto_SR1_Data.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>


class SkimSvc;

class Veto_SR1_Data : public Analysis {

public:
    Veto_SR1_Data();
    ~Veto_SR1_Data();

    void Initialize();
    void Execute();
    void Finalize();
    double zPosFromRow(double);
    double ODzCorr(double);
    bool ODNoise(float);
    float getXPosition(int);
    float getYPosition(int);

protected:

    CutsVeto_SR1_Data* m_cutsVeto_SR1_Data;
    ConfigSvc* m_conf;
    SkimSvc* m_skim;

    int n_events{0};
    int n_ss_events{0};
    int n_final_events{0};
    float s1Area{0.0};
    float s2Area{0.0};
    float sigma{0.0};

    int s1ID{-1};
    int s2ID{-1};

    bool cutIsMuon{false};
    bool cutMuonVeto{false};
    bool cutBuffers{false};
    bool cutExcessArea{false};
    bool cutBurst{false};
    bool livetimeCuts{false};
    bool skinCut{false};

    bool roiCut{false};
    bool fiducialCut{false};
    bool physicsCuts{false};

    bool cutS1Prom{false};
    bool cutStinger{false};
    bool cutS1TBA{false};
    bool cutHSC{false};
    bool cutChanTimingS1{false};
    bool s1Cuts{false};

    bool cutS2Width{false};
    bool cutNarrowS2{false};
    bool cutEarlyPeakS2{false};
    bool cutXY{false};
    bool cutS2TBA{false};
    bool s2Cuts{false};
    bool allCuts{false};

};

#endif
