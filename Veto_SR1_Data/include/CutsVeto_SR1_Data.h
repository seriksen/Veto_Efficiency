#ifndef CutsVeto_SR1_Data_H
#define CutsVeto_SR1_Data_H

#include "EventBase.h"

class CutsVeto_SR1_Data {

public:
    CutsVeto_SR1_Data(EventBase* eventBase);
    ~CutsVeto_SR1_Data();
    bool Veto_SR1_DataCutsOK();

    double zPosFromRow(double row);
    double ODzCorr(double z);
    bool ODNoise(float);
    float getXPosition(int channel);
    float getYPosition(int channel);
    float getZPosition(int channel);

    double CalculateNRBandDistance_SR1(double S1c, double logS2c);

    // Physics cuts
    bool ROI(float,float);
    bool SkinCut(vector<int>, int);

private:
    EventBase* m_event;
};

#endif
