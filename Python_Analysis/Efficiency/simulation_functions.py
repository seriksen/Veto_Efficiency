import numpy as np
import uproot as up
import awkward as ak
import tqdm
import matplotlib.pyplot as plt


def calculate_od_delayed_efficiency(r_file, bins=np.arange(0, 2000, 10)):
    
    skim = up.open(r_file)
    skim_events = skim['Events']
    skim_scatters = skim['Scatters']
    s1ids = skim_scatters['ss./ss.s1PulseID'].array()
    tpc_start_time = skim_events['pulsesTPC./pulsesTPC.pulseStartTime_ns'].array()
    od_phd = skim_events['pulsesODHG./pulsesODHG.pulseArea_phd'].array()
    od_start_time = skim_events['pulsesODHG./pulsesODHG.pulseStartTime_ns'].array()
    
    veto_eff = []

    for i in tqdm.tqdm(range(len(s1ids))):
        t0 = tpc_start_time[i][s1ids[i]]

        # Loop through OD pulses
        this_phd = od_phd[i]
        this_start_time = od_start_time[i]
        vetoed_pulses = []
        for j in range(len(this_phd)):

            # Coincidence and Pulse Area cut
            if this_phd[j] > 37.5:
                vetoed_pulses.append(j)

        if len(vetoed_pulses) > 0:
            time_differences = this_start_time[vetoed_pulses] - t0
            valid_veto_times = time_differences[np.where(time_differences > 0)]
            if len(valid_veto_times) > 0:
                veto_eff.append(min(valid_veto_times) / 1000)  # convert to us
    
    # turn result into array
    veto_eff = np.array(veto_eff)
    hist_values, bin_edges = np.histogram(veto_eff, bins)
    
    # Calculate the cumulative histogram using numpy.cumsum
    veto_efficiency = np.cumsum(hist_values) / len(s1ids)
    
    return veto_efficiency


def calculate_od_prompt_efficiency(r_file, bins=np.arange(0, 300, 10)):
    
    skim = up.open(r_file)
    skim_events = skim['Events']
    skim_scatters = skim['Scatters']
    s1ids = skim_scatters['ss./ss.s1PulseID'].array()
    tpc_start_time = skim_events['pulsesTPC./pulsesTPC.pulseStartTime_ns'].array()
    od_phd = skim_events['pulsesODHG./pulsesODHG.pulseArea_phd'].array()
    od_start_time = skim_events['pulsesODHG./pulsesODHG.pulseStartTime_ns'].array()
    
    veto_eff = []

    for i in tqdm.tqdm(range(len(s1ids))):
        t0 = tpc_start_time[i][s1ids[i]]

        # Loop through OD pulses
        this_phd = od_phd[i]
        this_start_time = od_start_time[i]
        vetoed_pulses = []
        for j in range(len(this_phd)):
            vetoed_pulses.append(j)

        if len(vetoed_pulses) > 0:
            time_differences = (this_start_time[vetoed_pulses] - t0)
            valid_veto_times = time_differences[np.where(time_differences > -300) and np.where(time_differences < 300)]
            if len(valid_veto_times) > 0:
                veto_eff.append(min(valid_veto_times))  # convert to us
    
    # turn result into array
    veto_eff = np.array(veto_eff)
    hist_values, bin_edges = np.histogram(veto_eff, bins)
    
    # Calculate the cumulative histogram using numpy.cumsum
    veto_efficiency = np.cumsum(hist_values) / len(s1ids)
    
    return veto_efficiency


def calculate_skin_prompt_efficiency(r_file, bins=np.arange(0, 300, 10)):
    
    skim = up.open(r_file)
    skim_events = skim['Events']
    skim_scatters = skim['Scatters']
    s1ids = skim_scatters['ss./ss.s1PulseID'].array()
    tpc_start_time = skim_events['pulsesTPC./pulsesTPC.pulseStartTime_ns'].array()
    skin_phd = skim_events['pulsesSkin./pulsesSkin.pulseArea_phd'].array()
    skin_start_time = skim_events['pulsesSkin./pulsesSkin.pulseStartTime_ns'].array()
    
    veto_eff = []

    for i in tqdm.tqdm(range(len(s1ids))):
        t0 = tpc_start_time[i][s1ids[i]]

        # Loop through OD pulses
        this_phd = skin_phd[i]
        this_start_time = skin_start_time[i]
        vetoed_pulses = []
        for j in range(len(this_phd)):

            # Coincidence Cut
            if this_phd[j] > 2.5:
                vetoed_pulses.append(j)

        if len(vetoed_pulses) > 0:
            time_differences = (this_start_time[vetoed_pulses] - t0)
            valid_veto_times = time_differences[np.where(time_differences > -500) and np.where(time_differences < 500)]
            if len(valid_veto_times) > 0:
                veto_eff.append(min(valid_veto_times))  # convert to us
    
    # turn result into array
    veto_eff = np.array(veto_eff)
    hist_values, bin_edges = np.histogram(veto_eff, bins)
    
    # Calculate the cumulative histogram using numpy.cumsum
    veto_efficiency = np.cumsum(hist_values) / len(s1ids)
    
    return veto_efficiency

def calculate_skin_delayed_efficiency(r_file, bins=np.arange(0, 2000, 10)):
    
    skim = up.open(r_file)
    skim_events = skim['Events']
    skim_scatters = skim['Scatters']
    s1ids = skim_scatters['ss./ss.s1PulseID'].array()
    tpc_start_time = skim_events['pulsesTPC./pulsesTPC.pulseStartTime_ns'].array()
    skin_phd = skim_events['pulsesSkin./pulsesSkin.pulseArea_phd'].array()
    skin_start_time = skim_events['pulsesSkin./pulsesSkin.pulseStartTime_ns'].array()
    
    veto_eff = []

    for i in tqdm.tqdm(range(len(s1ids))):
        t0 = tpc_start_time[i][s1ids[i]]

        # Loop through OD pulses
        this_phd = skin_phd[i]
        this_start_time = skin_start_time[i]
        vetoed_pulses = []
        for j in range(len(this_phd)):

            # Coincidence Cut
            if this_phd[j] > 50:
                vetoed_pulses.append(j)

        if len(vetoed_pulses) > 0:
            time_differences = this_start_time[vetoed_pulses] - t0
            valid_veto_times = time_differences[np.where(time_differences > 500)]
            if len(valid_veto_times) > 0:
                veto_eff.append(min(valid_veto_times) / 1000)  # convert to us
    
    # turn result into array
    veto_eff = np.array(veto_eff)
    hist_values, bin_edges = np.histogram(veto_eff, bins)
    
    # Calculate the cumulative histogram using numpy.cumsum
    veto_efficiency = np.cumsum(hist_values) / len(s1ids)
    
    return veto_efficiency