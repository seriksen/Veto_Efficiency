import numpy as np
import uproot as up
import awkward as ak
import tqdm
import matplotlib.pyplot as plt


def calculate_od_delayed_veto(r_file):
    
    skim = up.open(data_file)
    skim_events = skim['Events']
    skim_scatters = skim['Scatters']
    s1ids = skim_scatters['ss./ss.s1PulseID'].array()
    tpc_start_time = skim_events['pulsesTPC./pulsesTPC.pulseStartTime_ns'].array() + skim_events['pulsesTPC./pulsesTPC.areaFractionTime5_ns'].array()
    od_phd = skim_events['pulsesODHG./pulsesODHG.pulseArea_phd'].array()
    od_coincidence = skim_events['pulsesODHG./pulsesODHG.coincidence'].array()
    od_start_time = skim_events['pulsesODHG./pulsesODHG.pulseStartTime_ns'].array() + skim_events['pulsesODHG./pulsesODHG.areaFractionTime5_ns'].array()
    
    veto_eff = []

    for i in tqdm.tqdm(range(len(s1ids))):
        t0 = tpc_start_time[i][s1ids[i]]

        # Loop through OD pulses
        this_phd = od_phd[i]
        this_coincidence = od_coincidence[i]
        this_start_time = od_start_time[i]
        vetoed_pulses = []
        for j in range(len(this_phd)):

            # Coincidence and Pulse Area cut
            if this_phd[j] > 37.5 and this_coincidence[j] > 5:
                vetoed_pulses.append(j)

        if len(vetoed_pulses) > 0:
            time_differences = this_start_time[vetoed_pulses] - t0
            valid_veto_times = time_differences[np.where(time_differences > 0)]
            if len(valid_veto_times) > 0:
                veto_eff.append(min(valid_veto_times) / 1000)  # convert to us
    
    # turn result into array
    veto_eff = np.array(veto_eff)
    hist_values, bin_edges = np.histogram(veto_eff, np.arange(0,2000,10))
    
    # Calculate the cumulative histogram using numpy.cumsum
    cumulative_events_tagged = np.cumsum(hist_values)
    
    return cumulative_events_tagged


def calculate_od_prompt_veto(r_file):
    
    skim = up.open(data_file)
    skim_events = skim['Events']
    skim_scatters = skim['Scatters']
    s1ids = skim_scatters['ss./ss.s1PulseID'].array()
    tpc_start_time = skim_events['pulsesTPC./pulsesTPC.pulseStartTime_ns'].array() + skim_events['pulsesTPC./pulsesTPC.areaFractionTime5_ns'].array()
    od_phd = skim_events['pulsesODHG./pulsesODHG.pulseArea_phd'].array()
    od_coincidence = skim_events['pulsesODHG./pulsesODHG.coincidence'].array()
    od_start_time = skim_events['pulsesODHG./pulsesODHG.pulseStartTime_ns'].array() + skim_events['pulsesODHG./pulsesODHG.areaFractionTime5_ns'].array()
    
    veto_eff = []

    for i in tqdm.tqdm(range(len(s1ids))):
        t0 = tpc_start_time[i][s1ids[i]]

        # Loop through OD pulses
        this_phd = od_phd[i]
        this_coincidence = od_coincidence[i]
        this_start_time = od_start_time[i]
        vetoed_pulses = []
        for j in range(len(this_phd)):

            # Coincidence Cut
            if this_coincidence[j] > 5:
                vetoed_pulses.append(j)

        if len(vetoed_pulses) > 0:
            time_differences = (this_start_time[vetoed_pulses] - t0)
            valid_veto_times = time_differences[np.where(time_differences > -300) and np.where(time_differences < 300)]
            if len(valid_veto_times) > 0:
                veto_eff.append(min(valid_veto_times))  # convert to us
    
    # turn result into array
    veto_eff = np.array(veto_eff)
    hist_values, bin_edges = np.histogram(veto_eff, np.arange(0,300,10))
    
    # Calculate the cumulative histogram using numpy.cumsum
    cumulative_events_tagged = np.cumsum(hist_values)
    
    return cumulative_events_tagged