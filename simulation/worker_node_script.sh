#!/bin/bash

# Save machine OS for debugging
cat /etc/os-release
echo

# Save to ganga stdout
export PARTICLE="${PARTICLE}"
export SEED="${SEED}"
export OUTPUT_FILENAME="${OUTPUT_FILENAME}"
export OUTPUT_DIR="./"
export NBEAMON="${NBEAMON}"
export MACRO="${MACRO}"
export PYTHON_SCRIPT="${PYTHON_SCRIPT}"
export HDFS_NPY_OUTPUT_DIR="${HDFS_NPY_OUTPUT_DIR}"
export HDFS_OUTPUT_DIR="${HDFS_OUTPUT_DIR}"
export THIS_BACCARAT_VERSION="${THIS_BACCARAT_VERSION}"
export THIS_DER_VERSION="${THIS_DER_VERSION}"
export THIS_LZAP_VERSION="${THIS_LZAP_VERSION}"
export LZAP_STEERING_FILE="${LZAP_STEERING_FILE}"
export RUN_VERBOSE_ANALYSIS="${RUN_VERBOSE_ANALYSIS}"

echo "Environment Variables"
echo "export PARTICLE=${PARTICLE}"
echo "export NBEAMON=${NBEAMON}"
echo "export OUTPUT_FILENAME=${OUTPUT_FILENAME}"
echo "export OUTPUT_DIR=./"
echo "export MACRO=${MACRO}"
echo "export PYTHON_SCRIPT=${PYTHON_SCRIPT}"
echo "export SEED=${SEED}"
echo "export HDFS_NPY_OUTPUT_DIR=${HDFS_NPY_OUTPUT_DIR}"
echo "export HDFS_OUTPUT_DIR=${HDFS_OUTPUT_DIR}"
echo "export THIS_BACCARAT_VERSION=${THIS_BACCARAT_VERSION}"
echo "export THIS_DER_VERSION=${THIS_DER_VERSION}"
echo "export THIS_LZAP_VERSION=${THIS_LZAP_VERSION}"
echo "export LZAP_STEERING_FILE=${LZAP_STEERING_FILE}"
echo "export RUN_VERBOSE_ANALYSIS=${RUN_VERBOSE_ANALYSIS}"
echo

# Making output directories
echo "Working Dir: ${PWD}"
echo "ls of PWD"
ls
echo

# BACCARAT
echo "Running BACCARAT"
#source /cvmfs/lz.opensciencegrid.org/BACCARAT/release-6.3.4/x86_64-centos7-gcc8-opt/setup.sh
source /scratch/seriksen/geom_edits/baccarat/setup.sh
BACCARATExecutable $MACRO > verbose_${SEED}.log
echo "BACCARAT FINISHED"
echo "ls of PWD"
ls
echo "Removing logs"
rm *.log
echo "ls of PWD"
ls
echo

# Now run LZLama
source /cvmfs/lz.opensciencegrid.org/LZLAMA/release-3.4.0/x86_64-centos7-gcc8-opt/setup.sh
LZLAMA.bin --file-processing SR1 --output-dir ${OUTPUT_DIR} --output-filename ${OUTPUT_FILENAME}${SEED} ${OUTPUT_DIR}${OUTPUT_FILENAME}root_${SEED}.root
echo "LZLAMA FINISHED"
echo "ls of PWD"
ls
echo "Removing logs"
rm *.log
echo "ls of PWD"
ls
echo

# Transfer to hdfs
echo "Transferring LZap output to ${HDFS_LZAP_OUTPUT_DIR}"
#hdfs dfs -mkdir -p ${HDFS_OUTPUT_DIR}/BACCARAT
hdfs dfs -mkdir -p ${HDFS_OUTPUT_DIR}/LZLAMA

#hdfs dfs -copyFromLocal ${OUTPUT_DIR}${OUTPUT_FILENAME}root_${SEED}.root ${HDFS_OUTPUT_DIR}/BACCARAT/
hdfs dfs -copyFromLocal ${OUTPUT_DIR}${OUTPUT_FILENAME}${SEED}_lzap.root ${HDFS_OUTPUT_DIR}/LZLAMA/

echo "Removing final bits"
rm *.root
rm *.log
echo "Complete!"s