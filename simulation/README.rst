Simuluation
===========

The simulations run for this analysis are two parts;

1. BACCARAT energy deposit
   - These are used for fast calculations of neutron capture time

2. LZLAMA
   - These are used to calculate the efficiency

The simulation code uses ganga for the job submission setup for Bristol computing.

To run;

.. code-block:: sh

    ganga batch_submission.py


To adapt to other clusters consider the following;

* The final saving (via hdfs) will need to change. This may not be needed at all
* A container may be needed (eg shifter or singularity)
* The backend may need changing (from Local to Slurm)