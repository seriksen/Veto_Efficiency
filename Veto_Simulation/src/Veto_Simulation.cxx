#include "Veto_Simulation.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsVeto_Simulation.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

#include <iostream>
#include <fstream>

// Constructor
Veto_Simulation::Veto_Simulation()
        : Analysis()
{
    m_event->IncludeBranch("ss");
    m_event->IncludeBranch("pulsesTPC");
    m_event->IncludeBranch("pulsesSkin");
    m_event->IncludeBranch("pulsesODHG");
    m_event->Initialize();

    // Setup logging
    logging::set_program_name("Veto_Simulation Analysis");

    // Setup the analysis specific cuts.
    m_cutsVeto_Simulation = new CutsVeto_Simulation(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();

    // Create a Skim
    m_skim = new SkimSvc();

}

// Destructor
Veto_Simulation::~Veto_Simulation()
{
    delete m_cutsVeto_Simulation;
    delete m_skim;
}

// Initialize() -  Called once before the event loop.
void Veto_Simulation::Initialize()
{
    INFO("Initializing Veto_Simulation Analysis");

    m_skim->InitializeSkim(m_event, "skim_Veto_Simulation.root");
}

// Execute() - Called once per event.
void Veto_Simulation::Execute()
{

    n_events++;

    // Run through cuts
    if ( (*m_event->m_singleScatter)->nSingleScatters > 0 && (*m_event->m_tpcPulses)->nPulses > 0 ){

        n_ss_events++;

        s1Area = (*m_event->m_singleScatter)->s1Area_phd;
        s2Area = (*m_event->m_singleScatter)->s2Area_phd;

        if (s1Area < 300) {

            // Call SR1 NR band cut function
            sigma = m_cutsVeto_Simulation->CalculateNRBandDistance_SR3(s1Area, log10(s2Area));
            // sigma = m_cutsVeto_Simulation->CalculateNRBandDistance_SR3(s1Area, log10(s2Area));

            // Plot S1 vs S2 to make sure correct NR band is used
            m_hists->BookFillHist("S1-vs-logS2", 300, 0, 300, 50, 2.5, 5.5, s1Area, log10(s2Area));
            DEBUG("s1: " + std::to_string(s1Area));
            DEBUG("slogs2: " + std::to_string(log10(s2Area)));
            DEBUG("sigma: " + std::to_string(sigma));

            if (sigma < 1 && sigma > -1) {

                n_final_events++;

                // Now save event
                m_skim->SkimEvent(m_event);
                m_hists->BookFillHist("sigma-S1-vs-logS2", 300, 0, 300, 50, 2.5, 5.5, s1Area, log10(s2Area));
            }
        }

    } // if TPC SS

}


// Finalize() - Called once after event loop.
void Veto_Simulation::Finalize()
{
    INFO("Finalizing Veto_Simulation Analysis");

    std::cout << "Processing Summary" << std::endl;
    std::cout << "--------------" << std::endl;
    std::cout << "n_events:" << n_events << std::endl;
    std::cout << "n_ss_events:" << n_ss_events << std::endl;
    std::cout << "n_final_events:" << n_final_events << std::endl;
    std::cout << "--------------" << std::endl;
}
