#include "CutsVeto_Simulation.h"
#include "ConfigSvc.h"

CutsVeto_Simulation::CutsVeto_Simulation(EventBase* eventBase)
{
    m_event = eventBase;
}

CutsVeto_Simulation::~CutsVeto_Simulation()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsVeto_Simulation::Veto_SimulationCutsOK()
{
    // List of common cuts for this analysis into one cut
    return true;
}

double CutsVeto_Simulation::CalculateNRBandDistance_SR3(double S1c, double logS2c) {
    // This function takes a S1,S2 pair and returns the
    // n-sigma distance from the NR band at that S1 value
    // This uses a Gaussian fit to the NR band out to 300 phd S1

    if (S1c > 300.) {
        return -999.;
    }
    // array of NR band mean, width parameters
    double meanParameters[4] = {-15.58, 16.82, 0.0006816, 3.872};
    double plusWidthParameters[4] = {-14.95, 19, 0.000657, 3.913};

    double mean = meanParameters[0] / (S1c + meanParameters[1]) +
                  meanParameters[2] * S1c + meanParameters[3];
    double width = plusWidthParameters[0] / (S1c + plusWidthParameters[1]) +
                   plusWidthParameters[2] * S1c + plusWidthParameters[3] - mean;

    double distance = (logS2c - mean) / width;

    return distance;
}

double CutsVeto_Simulation::CalculateNRBandDistance_SR1(double S1c, double logS2c) {
    // This function takes a S1,S2 pair and returns the
    // n-sigma distance from the NR band at that S1 value
    // This uses a Gaussian fit to the NR band out to 300 phd S1

    if (S1c > 300.) {
        return -999.;
    }
    // array of NR band mean, width parameters
    double meanParameters[4] = {-16.9133, 18.010276, 6.7732e-4, 4.00261};
    double plusWidthParameters[4] = {-16.2913996, 22.9237069, 6.6466e-4, 4.06502};

    double mean = meanParameters[0] / (S1c + meanParameters[1]) +
                  meanParameters[2] * S1c + meanParameters[3];
    double width = plusWidthParameters[0] / (S1c + plusWidthParameters[1]) +
                   plusWidthParameters[2] * S1c + plusWidthParameters[3] - mean;

    double distance = (logS2c - mean) / width;

    return distance;

}
