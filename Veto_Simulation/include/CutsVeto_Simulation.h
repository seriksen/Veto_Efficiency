#ifndef CutsVeto_Simulation_H
#define CutsVeto_Simulation_H

#include "EventBase.h"

class CutsVeto_Simulation {

public:
    CutsVeto_Simulation(EventBase* eventBase);
    ~CutsVeto_Simulation();
    bool Veto_SimulationCutsOK();

    // SR3 NR Band cut
    double CalculateNRBandDistance_SR1(double S1c, double logS2c);
    double CalculateNRBandDistance_SR3(double S1c, double logS2c);


private:
    EventBase* m_event;
};

#endif
