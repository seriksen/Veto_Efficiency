#ifndef Veto_Simulation_H
#define Veto_Simulation_H

#include "Analysis.h"

#include "CutsVeto_Simulation.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class Veto_Simulation : public Analysis {

public:
    Veto_Simulation();
    ~Veto_Simulation();

    void Initialize();
    void Execute();
    void Finalize();


protected:
    CutsVeto_Simulation* m_cutsVeto_Simulation;
    ConfigSvc* m_conf;
    SkimSvc* m_skim;

    int n_events{0};
    int n_ss_events{0};
    int n_final_events{0};
    float s1Area{0.0};
    float s2Area{0.0};
    float sigma{0.0};
};

#endif
